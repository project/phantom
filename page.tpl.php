<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $language->language ?>" xml:lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>" id="html-main">
<head>
  <title><?php echo $head_title ?></title>
  <?php echo $head ?>
  <?php echo $styles ?>
  <?php echo $scripts ?>
  <script type="text/javascript"></script>
</head>
<body>

<div class="pagebg">
  <div class="pagebg_img">
  </div>
  <?php if ($site_name) { ?><h3 id='sitetitle'><?php print $site_name ?></h3><?php } ?>
</div>
<!--Start Page-->
<div class="page_wrap">
  <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b></b>
  <div class="page">
  <?php if ($search_box): ?>
    <div id="search-box">
      <?php print $search_box; ?>
    </div><!-- /search-box -->
    <span class="small">&nbsp;</span>
  <?php endif; ?>
  </div>
</div>
<div class="page_wrap">
  <?php if ($logo) { ?>
    <div id="logo-picture" >
      <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
    </div><?php } ?>
  <div class="header">
  </div>
</div>
<div class="page_wrap">
  <!--Start primarymenu-->
  <?php if (isset($primary_links)) : ?>
  <div class="topmenu">
  <!-- admin edit   -->
  <?php if ($is_admin): ?><?php echo l(t("Edit menu"), "admin/build/menu-customize/primary-links", array("attributes" => array("class" => "edit-this-link"))); ?><?php endif; ?>
  <!-- admin edit   -->
    <?php echo theme('links', $primary_links) ?>
  <?php endif; ?>
  </div>
  <!--End primarymenu-->
  <div class="page">
    <?php print $breadcrumb; ?>
    <div class="clear-both"></div>
    <!--Start col_left-->
    <?php if ($left): ?>
    <div class="col_left">
      <?php echo $left ?>
    </div>
    <?php endif; ?>
    <!--End col_left-->
    <!--Start col_center-->
    <div class="col_center<?php if (empty($right) AND !empty($left)) {echo '_78';} if (empty($right) AND empty($left)) {echo '_97';} if (!empty($right) AND empty($left)) {echo '_66';} ?>">
      <div class="main-content-wrap">
        <?php if ($show_messages): ?>
        <?php echo $messages; ?>
        <?php endif; ?>
        <?php if ($title): ?><h1 class="title"><?php echo $title ?></h1><?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php echo $tabs; ?></div><?php endif; ?>
        <!-- main-content-block -->
        <div class="main-content-block">
        <?php echo $content; ?>
        <!-- content after blocks -->
        <?php if ($content_after_blocks): ?>
        <div class="content_after_blocks">
        <?php echo $content_after_blocks ?>
        </div>
        <?php endif; ?>
        <!-- after content block end-->
        </div>
      </div>
    <!-- /main-content-block -->
    </div>
    <!--End col_center-->
    <!--Start col_right-->
    <?php if ($right): ?>
    <div class="col_right">
    <?php echo $right ?>
    </div>
    <?php endif; ?>
    <!--End col_right-->
    <div class="clear-both">
    </div>
  </div>
  <b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b></b>
</div>
<!--End Page-->
<!--Start footer-->
<div class="page_wrap">
  <b class="xtop"><b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b></b>
  <div class="footer">
  <?php if ($footer_block): ?>
  <!-- /footer-blocks -->
    <div class="clear-both">
    </div>
    <div class="footer-blocks">
    <?php if ($footer_block): ?><?php echo $footer_block ?><?php endif; ?>
    </div>
  <!-- /footer-blocks -->
  <?php endif; ?>
  <div class="clear-both">
  <?php echo $footer ?>
  <?php echo $footer_message ?>
  <span class="footer_small"><a href="http://wyltstyle.com" title="Theme Design: wyltstyle">a :wyltstyle: theme</a></span>
  </div>
  <div class="clear-both">&nbsp;</div>
  <!--End footer-->
  </div>
</div>
<?php echo $closure ?>
</body>
</html>