<?php
?>

<?php if ($page == 0) { ?><h1 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h1><?php }; ?>
  <div class="submitted"><?php print $submitted?></div>
  <div class="node<?php if ($sticky&&$page == 0) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture && in_array($node->type, array('blog'))) print $picture; // only want picture on certain content types ?>
    <div class="content clear-block"><?php print $content?></div>
    <?php if ($terms) { ?><div class="taxonomy"><?php print $terms?></div><?php }; ?>
    <?php if ($links) { ?><div class="links"><?php print $links?></div><?php }; ?>
  </div>